const mongoose = require("mongoose");
const config = require("config");

const dbUri = config.get("mongoURI");

const connectDB = async () => {
  try {
    await mongoose.connect(dbUri, {
      useCreateIndex: true,
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("MongoDB Connected...");
  } catch (err) {
    console.error(err.message);
    //Exit process with error status
    process.exit(1);
  }
};

module.exports = connectDB;
